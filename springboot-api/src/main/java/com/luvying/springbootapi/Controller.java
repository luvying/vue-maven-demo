package com.luvying.springbootapi;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    public static final String API_PATH = "/api";

    @GetMapping(API_PATH + "/getData")
    public String getData(@RequestParam(required = false) String query){
        System.out.println("getData-params: " + query);
        return "{'user':'Mike','pwd':'12346'}";
    }

    @PostMapping(API_PATH + "/postData")
    public String postData(@RequestBody(required = false) String data){
        return "received:" + data;
    }

}
