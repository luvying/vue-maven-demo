# vue-maven-demo

#### 介绍
用maven插件```frontend-maven-plugin```将前后端分离项目(springboot + vue)自动打包合并为单体项目的demo

```frontend-maven-plugin```插件工作流程:
1.  安装node,npm
2.  安装前端项目依赖
3.  编译前端项目

重点见三个pom的配置

前端项目简单写了axios、router的demo,打包合并后运行正常

#### 软件架构
```
vue-maven-demo
├──springboot-api
└── vue-web
```
vue-maven-demo  父模块

springboot-api  后端模块

vue-web         前端模块

#### 使用说明
开发时是以前后端分离的方式进行开发：
1.  后端运行springboot-api,端口8080
2.  前端运行 vue-web ```npm run serve```,端口8000

打包时将会编译前端项目并复制到后端项目中：
1.  在```vue-maven-demo```中```maven install```,注意maven版本为3.6.0
2.  打包后的jar包将在```vue-maven-demo\springboot-api\target```下
3.  运行jar包```java -jar xxx.jar```,访问8080端口即可
