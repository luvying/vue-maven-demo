import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../layout/index'

Vue.use(Router)

// 处理进入同一个路由出现NavigationDuplicated的问题
const originalPush = Router.prototype.push;
Router.prototype.push = function push (location) {
    return originalPush.call(this, location).catch(err => err)
};

// 公共路由
export const constantRoutes = [
    {
        // 根路由重定向到index
        path: '/',
        component: Layout,
        redirect: 'index',
        children: [
            {
                path: 'index',
                name: 'Index',
                component: () => import('@/views/index'),
                meta: {title: '首页', icon: 'dashboard', affix: true}
            }, {
                path: 'requestDemo',
                name: 'requestDemo',
                component: () => import('@/components/RequestDemo'),
            }, {
                path: 'hello',
                name: 'hello',
                component: () => import('@/components/HelloWorld'),
            },
        ]
    }, {
        path: '/404',
        component: () => import('@/views/error/404'),
        hidden: true
    }
]


export default new Router({
    // mode: 'hash',
    mode: 'history', // 去掉url中的#
    scrollBehavior: () => ({y: 0}),
    routes: constantRoutes
})