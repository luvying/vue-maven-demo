import request from '@/utils/request'

//请求示例
//get 调用: getData({userId:'123',userPwd:'456'}).then(res=>{alert(res);});
export const getData = (query) => {
    return request({
        url: "/getData",
        method: "get",
        params: query
    })
}
//post
export const postData = (data) => {
    return request({
        url: "/postData",
        method: "post",
        data: data
    })
}