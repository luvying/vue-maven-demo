import axios from 'axios'
import {Message, Loading} from 'element-ui'

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'

const service = axios.create({
    // baseURL: process.env.BASE_API, // api 的 base_url use proxy table
    // URL公共部分
    baseURL: "/api", // api 的 base_url use proxy table
    timeout: 10000, // 请求超时10秒
    headers: {
        'Content-Type': 'application/json;charset=UTF-8;',
    }
})

// request拦截器
service.interceptors.request.use(config => {
    // 打开Loading
    Loading.service();

    // 将相关认证信息添加至header

    // get请求映射params参数, 将参数拼接在url后
    if (config.method === 'get' && config.params) {
        let url = config.url + '?' + tansParams(config.params);
        url = url.slice(0, -1);
        config.params = {};
        config.url = url;
    }
    return config
}, error => {
    Loading.service().close();
    console.log(error);
    Promise.reject(error);
})

// 响应拦截器
service.interceptors.response.use(res => {
        // 关闭Loading
        Loading.service().close();
        // 未设置状态码则默认成功状态
        const code = res.data.code || 200;
        // 获取错误信息
        const msg = errorCode[code] || res.data.message || errorCode['default'];
        // 二进制数据则直接返回
        if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
            return res.data
        }
        if (code === 401) {
            Message({message: '登录状态已过期, 请重新登陆', type: 'warning'});
            // 路由跳转登录页面
            // router.push('/login');
            return Promise.reject('无效的会话，或者会话已过期，请重新登录。')
        } else if (code === 403) {
            // 用户访问了无权限的资源,跳转到首页
            Message({message: '无访问权限', type: 'error'});
            // router.push('/');
            return Promise.reject('error');
        } else if (code === 500) {
            Message({
                message: msg,
                type: 'error'
            });
            return Promise.reject(new Error(msg))
        } else if (code !== 200) {
            Notification.error({
                title: msg
            });
            return Promise.reject('error')
        } else {
            // 返回data
            return res.data
        }
    },
     error => {
         Loading.service().close();
        console.log('err' + error);
        let {message} = error;
        if (message === "Network Error") {
            message = "后端接口连接异常";
        } else if (message.includes("timeout")) {
            message = "系统接口请求超时";
        } else if (message.includes("Request failed with status code")) {
            message = "系统接口" + message.substr(message.length - 3) + "异常";
        }
        Message({
            message: message,
            type: 'error',
            duration: 5 * 1000
        });
        return Promise.reject(error)
    }
)

const errorCode = {
    '401': '认证失败，无法访问系统资源',
    '403': '当前操作没有权限',
    '404': '访问资源不存在',
    'default': '系统未知错误，请反馈给管理员'
}

/**
 * 参数处理
 * @param {*} params  参数
 */
export function tansParams(params) {
    let result = ''
    for (const propName of Object.keys(params)) {
        const value = params[propName];
        var part = encodeURIComponent(propName) + "=";
        if (value !== null && typeof (value) !== "undefined") {
            if (typeof value === 'object') {
                for (const key of Object.keys(value)) {
                    if (value[key] !== null && typeof (value[key]) !== 'undefined') {
                        let params = propName + '[' + key + ']';
                        var subPart = encodeURIComponent(params) + "=";
                        result += subPart + encodeURIComponent(value[key]) + "&";
                    }
                }
            } else {
                result += part + encodeURIComponent(value) + "&";
            }
        }
    }
    return result
}

// 通用GET请求
export const get = (url, data, config) => {
    return service({
        url: url,
        method: "get",
        params: data,
        config: config
    })
}

// 通用POST请求
export const post = (url, data, config) => {
    return service({
        url: url,
        method: "post",
        data: data,
        config: config
    })
}

export default service
