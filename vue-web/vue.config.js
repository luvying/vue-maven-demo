module.exports = {
    // 前端打包进后端,因此仅在开发阶段需要指定后端地址,生产环境不使用nodejs
    devServer: {
        //vue启动端口
        port: 8000,
        proxy: {
            '/api': {
                // 添加代理,即后端的地址,发送的请求url会变成前端的ip和端口,就不会暴露后端的地址了 将 /api 替换成
                target: 'http://127.0.0.1:8080/api',
                // 允许跨域
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    // 上面的target会变成 /api/xxx,即axios调用的url时应填写 /api/xxx
                    '^/api': '/'
                }
            }
        }
    }
}